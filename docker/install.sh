#!/usr/bin/env bash

# install required packages
sudo pacman --sync --needed --noconfirm \
    docker \
    docker-machine \
    docker-compose

# post installation steps
sudo groupadd docker &
sudo usermod --append --groups docker $USER &

#
mkdir --parents "$HOME/.docker"

sudo chown --recursive $USER:$USER "$HOME/.docker"
sudo chmod --recursive g+rwx "$HOME/.docker"

# enable on startup
sudo systemctl enable docker
