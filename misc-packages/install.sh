#!/usr/bin/env bash

base_packages=(
	xorg-server xorg-xset
	gnome-keyring seahorse
	gnome-terminal gnome-tweaks gnome-clocks
	blueman
	rsync
	ack
	ncdu
	man-db
	netcat
	bat
	rofi rofi-calc
	reflector
	firefox
	ranger
	exfat-utils
	gdb
	tree
	xdotool
	zathura zathura-pdf-mupdf
	signal-desktop
	nextcloud-client
	keepassxc
	unzip
	stow
	curl
	wget
	the_silver_searcher
	virt-viewer
	system-config-printer
	avahi
	cups lib32-libcups
	dnsutils
	whois
	ntfs-3g
	icedtea-web
	gvfs-smb
	libcanberra
	nemo nemo-share nemo-seahorse nemo-preview nemo-fileroller
	libreoffice-still
	wmctrl
	zbarimg
	nerd-fonts-hack
	vlc
	cronie
	eza
	ripgrep
	vpnc networkmanager-vpnc
	plasma-systemmonitor
	android-file-transfer
	baobab
	net-tools nmap
	htop
	tesseract tesseract-data-eng
	nodejs-emojione noto-fonts-emoji
	ctags
	obsidian
	syncthing
	pass
	pavucontrol
	android-file-transfer
	gvfs-smb
	zbar-tools
	debugedit
	zsh
)

aur_packages=(
	git-extras
	gnome-shell-pomodoro
	teamviewer
	tmuxinator
	jetbrains-toolbox
	discord
	flameshot-git
	thunderbird
	xclip-git
	fzf-extras
	spotify
	teams-for-linux-bin
	xviewer
	dragon-drop
	downgrade
	insomnia
	brave-bin
	windsurf
	jq
	nvm
	pnpm
)

for package in "${base_packages[@]}"; do
  figlet "$package"
  sudo pacman --sync --needed --noconfirm ${package} > /dev/null
done

for package in "${aur_packages[@]}"; do
  figlet "$package"
	yay --sync --needed --noconfirm --norebuild --noredownload ${package} 1> /dev/null
done
