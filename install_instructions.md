#!/usr/bin/env bash

##### upgrade package database
```pacman -Syyy```

##### (optional) change mirror
```vim /etc/pacman.d/mirrorlist```

##### enter fdisk
```fdisk -l```

##### enter fdisk of your harddrive
```fdisk /dev/sdx```

##### create new GPT table (optional)
```g```

##### create new partition
```n, enter ```
##### give size in (last sector)
+500M

##### specify type
```t```

##### 1 for EFI system
```1```

##### create second partition for LVM (30)
```n, enter, enter, t, 30```

##### write changes
```w```

##### format EFI partition, where x is first partition
```mkfs.fat -F32 /dev/sdx```

##### create physical volume, dataalignment beneficial for ssds, where x stands for second partition
```pvcreate --dataalignment 1m  /dev/sdax```

##### create volume group, where x is second partition
```vgcreate systemvg /dev/sdax```

##### create logical volume for root
```lvcreate -L 30GB -n rootlv systemvg```

##### create logical volume for home , 100%FREE means everything else will be used
```lvcreate -l 100%FREE -n homelv systemvg```

##### refresh lvm module
```modprobe dm_mod```

##### scan volume groups
```vgscan```

##### activate volume groups
```vgchange -ay```

##### format logical volume, where x is second partition
```mkfs.ext4 /dev/systemvg/rootlv```

```mkfs.ext4 /dev/systemvg/homelv```

##### mount logical volume 

##### root
```mount /dev/systemvg/rootlv /mnt```

##### home
```mkdir -p /mnt/home```
```mount /dev/systemvg/rootlv /mnt/home```

##### create etc dir
```mkdir /mnt/etc```

##### generate fstab
```genfstab -Up /mnt >> /mnt/etc/fstab```

##### install base packages for running system
```pacstrap -i /mnt base```

##### chroot into system
```arch-chroot /mnt```

##### install linux kernel and headers
##### (optional) stable keren
##### pacman -S linux-lts linux-lts-headers
```pacman -S linux linux-headers```

##### IMPORTANT! install vim :-D
```pacman -S vim ```

##### install some base packages
```pacman -S base-devel```

##### install networkmanager
```pacman -S networkmanager```
##### (optional) WIFI
```pacman -S wpa_supplicant wireless_tools netctl```

##### enable networkmanager on startup
```systemctl enable NetworkManager```

##### install dialog package
```pacman -S dialog```

##### install lvm2 package
```pacman -S lvm2```

##### change mkinitcpio.conf for lvm in kernel
```vim /etc/mkinitcpio.conf```
##### change HOOKS=(base udev autodetect modconf block filesystems keyboard fsck) to
##### HOOKS=(base udev autodetect modconf block **lvm2** filesystems keyboard fsck) 

##### refresh kernel
```mkinitcpio -p linux```

```vim /etc/locale.gen```
##### uncomment en_US.UTF-8 UTF-8

##### generate locale
```locale-gen```

##### create root password
```passwd```

##### create user group
```useradd -m -g users -G wheel USERNAME```

##### create user password
```passwd USER```

##### (optional) install sudo
```pacman -S sudo```

##### set EDITOR variable
```EDITOR=vim```

##### enter sudoers file
```visudo```
##### uncomment wheel ALL=(ALL) ALL

##### install grub boot packages
```pacman -S grub efibootmgr dosfstools os-prober mtools```

##### create EFI dir
```mkdir /boot/EFI```

##### mount partition to EFI dir, where x is first partition
```mount /dev/sdx /boot/EFI```

##### install grub on physical device
```grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck```

##### create grub locale dir
```mkdir /boot/grub/locale```

##### copy .mo file to new dir
```cp /usr/share/locale/en/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo```

##### apply config
```grub-mkconfig -o /buut/grub/grub.cfg```

##### locale space for spwapfile

```fallocate -l 2G /spwapfile```
```chmod 600 /spwapfile```
```mkswap /swapfile```

##### create backup from fstab file
```cp /etc/fstab etc/fstab.bak```

##### create entry for swapfile in fstab
```echo '/swapfile none swap sw 0 0' | tee --a /etc/fstab ```

##### install microcode for amd/intel
```pacman -S amd-ucode```
##### pacman -S intel-ucode

##### install xorg server
```pacman -S xorg-server xorg-xset```

##### (optional) AMD drivers
##### pacman -S mesa
##### install nvidia drivers, lts for stable
```pacman -S nvidia```

##### exit chroot env
```exit```
##### unmount all
```umount -a ```

##### reboot and check if every is correct
```reboot```

##### it could be that NetworkManager needs to be enabled again
```systemctl enable NetworkManager```
