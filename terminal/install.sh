#!/usr/bin/env bash

# set initial oh-my-zsh directory
ZSH_CUSTOM="$XDG_CONFIG_HOME/.oh-my-zsh"

# install fonts
yay --sync --needed --noconfirm --norebuild --noredownload \
	nerd-fonts-fira-code

# zsh-completions installation
[[ ! -d "$ZSH_CUSTOM/plugins/zsh-completions" ]] && {
       figlet "zsh-completions"
       git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-completions
}

# zsh-autosuggestions installation
[[ ! -d "$ZSH_CUSTOM/plugins/zsh-autosuggestions" ]] && {
       figlet "zsh-autosuggestions "
       git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
}

# warhol installation
[[ ! -d "$ZSH_CUSTOM/warhol" ]] && {
       figlet warhol
       git clone https://github.com/unixorn/warhol.plugin.zsh.git ${ZSH:-~/.config/.oh-my-zsh/}/plugins/warhol
}

# fzf installation
[[ ! -d "$HOME/.fzf" ]] && {
       figlet fzf
       git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
       bash ~/.fzf/install
}

# remove old .zshrc 
sudo rm -rf "$HOME/.zshrc"

# change to private dev dir
[[ ! -d "$HOME/Developing/private" ]] && {
	mkdir -p "$HOME/Developing/private"
}

cd "$HOME/Developing/private"

# grab dot-files repository
[[ ! -d 'dot-files' ]] && {
	figlet dot-files
	git clone https://gitlab.com/AndreasMurk/dot-files.git
}
# delete default .profile
rm -rf "$HOME/.profile"

cd "dot-files"

# symlink all files with stow
stow -t ~ $(ls -d */) -v

# source .zshrc
source "$HOME/.config/zsh/.zshrc"
