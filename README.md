# install-arch

This repository is used to install all required packages for a running system with Arch Linux including a cinnamon desktop environment.

Apart from that, additional packages for the daily use are included in several sub-directories. 

# Install LVM

- open Terminal with CTRL + ALT + T
- change to root `sudo su -`
- open GParted `gparted` or over GUI
- create first partition with enough space for `/` and `/home`
- create second partition with enough space for `/boot` (approximately 1024 byte)
- apply changes
- create an LVM physical volume, a volume group and three logical volumes `sudo pvcreate /dev/abc` 
`vgcreate systemvg /dev/abc` 
- create Logical volume groups with : 
    - `lvcreate -n homelv -L 200g systemvg` 
    - `lvcreate -n rootlv -L 200g systemvg` 
    - `lvcreate -n swaplv -L 8g systemvg`
- ![LVM](lvm_2.png)
- start regular installer
- format and assign partitions to `/`, `/home`, `/boot` and `swap`
- assign `/boot` to non-lvm partition
 - ![LVM](lvm.png)


For further instructions on how to setup Linux Mint with LVM + LUKS encryption, please refer to these tutorials combined:
- https://nowhere.dk/articles/installing-linux-mint-ubuntu-desktop-edition-with-full-disk-encryption-and-lvm
- https://askubuntu.com/questions/293028/how-can-i-install-ubuntu-encrypted-with-luks-with-dual-boot

