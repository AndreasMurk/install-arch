#!/usr/bin/env bash

# install nvim packages
sudo pacman --sync --needed --noconfirm \
    neovim \
    python-pynvim \

# install vim-plug for plugin management
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
