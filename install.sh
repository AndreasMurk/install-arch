#!/usr/bin/env bash
  
# update system
sudo pacman --sync --refresh --sysupgrade --needed --noconfirm 1> /dev/null

# install figlet for ascii banners
sudo pacman --sync --needed --noconfirm figlet 1> /dev/null

packages=(
	yay
	display-packages
	zsh
	misc-packages
	vim
	docker
	terminal
	base16
	tmux
)

for package in "${packages[@]}"; do
	# ascii banner for each package
	figlet "$package"
	bash ${package}/install.sh 
done
