#!/usr/bin/env bash

DOT_DIR="$DEV/dot-files"

# install packages
sudo pacman --sync --needed --noconfirm tmux 1> /dev/null

# clone repository
[[ ! -d "$HOME/.tmux" ]] && {
  git clone https://github.com/gpakosz/.tmux.git
  ln -s -f ".tmux/.tmux.conf"
}

# source files
tmux 
tmux source-file "$HOME/.tmux.conf.local"
