#!/usr/bin/env bash

# change default shell to zsh
sudo usermod --shell $(command -v zsh) $USER

# install oh-my-zsh to .config/oh-my-zsh
ZSH=~/.config/.oh-my-zsh sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" &

# powerlevel10k theme
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.config/.oh-my-zsh/custom}/themes/powerlevel10k
