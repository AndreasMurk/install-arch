#!/usr/bin/env bash

display_packages=(
	papirus-icon-theme
	arc-gtk-theme
	archlinux-wallpaper
	powerline
)

packages=(
	lightdm lightdm-settings
	lightdm-slick-greeter
	cinnamon
	powerline-fonts-git
	ttf-droid
	ttf-firacode-nerd
	zsh-theme-powerlevel10k
)

for package in "${display_packages[@]}"; do
  figlet "$package"
  sudo pacman --sync --needed --noconfirm ${package} > /dev/null
done

for package in "${packages[@]}"; do
  figlet "$package"
	yay --sync --needed --noconfirm --norebuild --noredownload ${package} 1> /dev/null
done

echo "Do not forget running `sudo systemctl enable lightdm.service`"


