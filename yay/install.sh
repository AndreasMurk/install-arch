#! /usr/bin/env bash

# configure git
git clone https://aur.archlinux.org/yay.git /tmp/yay

# go into directory
cd /tmp/yay

# build package
makepkg -si

# remove directory
rm -rf /tmp/yay
